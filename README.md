# ddk-client-nodejs

#### 介绍
拼多多-多多进宝nodejs-sdk简单实现.

#### 安装教程

npm i request crypto

#### 使用说明
const PddClient = require('/lib/ddk/client')

const pddClient = new PddClient({clientId:'xxx',appSecret:'xxx'})

pddClient.execute('pdd.ddk.goods.search', {activity_tags: [4]})
.then(res=>{console.log(res)})
.catch(res=>{console.warn('ops, ', res)})

